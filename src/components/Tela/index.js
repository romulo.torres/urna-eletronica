import "./styles.css";
import urnaConfigs from "../../urna-configs.json";
import { useEffect, useState } from "react";

export function Tela({ etapa, valor }) {
  console.log("urnaConfigs", urnaConfigs);
  const [eleitor, setEleitor] = useState(null);

  useEffect(() => {
    const _eleitor = urnaConfigs.eleitores.find(
      (eleitor) => eleitor.Id === valor
    );
    if (_eleitor) {
      setEleitor(_eleitor);
    } else {
      setEleitor(null);
    }
  }, [valor]);

  return (
    <div className="tela">
      Tela
      <br />
      {valor}
      {eleitor && eleitor.Nome}
    </div>
  );
}
