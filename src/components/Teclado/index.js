import "./styles.css";

export function Teclado({ setValor, limparValor, confirmar }) {
  const botoesNumericos = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"];
  const botoesFuncoes = {
    CORRIGIR: () => {
      limparValor();
    },
    BRANCO: () => {},
    CONFIRMA: () => {
      confirmar();
    },
  };
  return (
    <div className="teclado">
      {botoesNumericos.map((botao, index) => (
        <button
          className={`botao botao-${botao}`}
          key={index}
          onClick={() => setValor(botao)}
        >
          {botao}
        </button>
      ))}
      <hr />
      {Object.keys(botoesFuncoes).map((botao, index) => (
        <button
          className={`botao botao-funcao botao-${botao.toLowerCase()}`}
          key={index}
          onClick={() => botoesFuncoes[botao]()}
        >
          {botao}
        </button>
      ))}
    </div>
  );
}
