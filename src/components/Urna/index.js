import { useState } from "react";
import { Teclado } from "../Teclado";
import { Tela } from "../Tela";
import "./styles.css";

export function Urna({ children }) {
  const [etapa, setEtapa] = useState(0);
  const [valor, setValor] = useState("");

  const updateValor = (numero) => {
    setValor(`${valor}${numero}`);
  };

  const limparValor = (numero) => {
    setValor("");
  };

  const confirmar = () => {
    const report = {
      votou: ["1234", "5678", "9012", "3456", "7890"],
      lanche: {
        1234: 2,
        9012: 3,
      },
      bebida: {
        123: 1,
        345: 4,
      },
    };

    const _report = JSON.parse(localStorage.getItem("REPORT"));
    console.log("_report", _report);
    //localStorage.setItem("REPORT", JSON.stringify(report));

    //localStorage.removeItem("MEU VALOR");
  };

  return (
    <div className="urna">
      <Tela valor={valor} etapa={etapa} />
      <Teclado
        setValor={updateValor}
        limparValor={limparValor}
        confirmar={confirmar}
      />
    </div>
  );
}
